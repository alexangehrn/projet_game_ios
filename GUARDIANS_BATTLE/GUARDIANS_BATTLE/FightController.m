//
//  FightController.m
//  GUARDIANS_BATTLE
//
//  Created by alexandra angehrn on 29/11/2016.
//  Copyright © 2016 alexandra angehrn. All rights reserved.
//

#import "FightController.h"
#import "AttacksController.h"

@interface FightController ()

@end

@implementation FightController
- (IBAction)specialAttackButton:(id)sender {
    AttacksController *rc =[self.storyboard instantiateViewControllerWithIdentifier:@"attacks"];
    [self.navigationController pushViewController:rc animated:YES];
}
- (IBAction)SimpleAttackButton:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
